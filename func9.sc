import scala.io.Source

def func1(): Unit ={
	//Gets the entire file into csVRow then outputs entire contents
	val source = scala.io.Source.fromFile("test.csv")
	val csVRow = try source.mkString finally source.close()
	println(csVRow)

	//Goes through each line(if needed), also seperates each value and places into list to use for other purposes

	for (line <- Source.fromFile("test.csv").getLines) {
	val x = line.split(',')
	println(x.toList)
	}
	}
func1()
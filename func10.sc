//the toString() method is built into scala

class MyShape{
object Shape{
	
   var color = "red"
   var filled = true

   def Shape() : Unit ={
   }
   def Shape(s_color:String,s_filled:Boolean) : Unit={
   		color = s_color
   		filled = s_filled
   }
   def getColor() : String ={
   		return color
   }
   def setColor(s_color:String) : Unit={
   		color = s_color
   }
   def isFilled() : Boolean ={
   		return filled
   }
   def setFilled(s_filled:Boolean) : Unit={
   		filled = s_filled
   }
}
}
class MyCircle extends MyShape{
object Circle{
	var radius = 1.0

	def Circle() : Unit={
	}
	def Circle(s_radius:Double) : Unit={
		radius = s_radius
	}
	def Circle(s_radius:Double,s_color:String,s_filled:Boolean) : Unit={
		radius = s_radius
		Shape.setColor(s_color)
		Shape.setFilled(s_filled)
	}
	def getRadius():Double ={
		return radius
	}
	def setRadius(s_radius : Double) :Unit={
		radius = s_radius
	}
	def getArea():Double={
		return (3.14*radius*radius)
	}
	def getPerimeter():Double={
		return(2*3.14*radius)
	}
}
}
class MyRectangle extends MyShape{
object Rectangle{
	var width = 1.0
	var length = 1.0

	def Rectangle() : Unit={
	}
	def Rectangle(s_width:Double,s_length:Double) : Unit ={
		width = s_width
		length = s_length
	}
	def Rectangle(s_width:Double,s_length:Double,s_color:String,s_filled:Boolean){
		width = s_width
		length = s_length
		Shape.setColor(s_color)
		Shape.setFilled(s_filled)
	}
	def getWidth():Double={
		return width
	}
	def setWidth(s_width:Double):Unit={
		width = s_width
	}
	def getLength():Double={
		return length
	}
	def setLength(s_length:Double):Unit={
		length = s_length
	}
	def getArea():Double={
		return (length * width)
	}
	def getPerimeter():Double={
		return (length + length + width + width)
	}
}
}

class MySquare extends MyRectangle{
	object Square{
		var side = 1.0

		def Square():Unit={
		}
		def Square(s_side:Double):Unit={
			side = s_side

		}
		def Square(s_side:Double,s_color:String,s_filled:Boolean) : Unit={
			side = s_side
			Shape.setColor(s_color)
			Shape.setFilled(s_filled)
		}
		def getSide():Double={
			return side
		}
		def setSide(s_side:Double):Unit={
			side = s_side
		}
		def setWidth(s_width:Double):Unit={
			Rectangle.setWidth(s_width)
		}
		def setLength(s_length:Double):Unit={
			Rectangle.setLength(s_length)
		}
	}
}
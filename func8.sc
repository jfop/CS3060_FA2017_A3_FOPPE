import scala.io.Source
import scala.collection.mutable.ListBuffer

def func1(a: String, b: String): Int ={
a.zip(b).count(c => c._1 != c._2)
}

var myNewList = new ListBuffer[String]()

val filename = "test.txt"
for (line <- Source.fromFile(filename).getLines) {
	val x = line.split(':')

    println(x.toList)
    val a = x(0)
    val b = x(1)
    val result = func1(a,b)

    myNewList += a
    myNewList += ":"
    myNewList += b
    myNewList += ":"
    myNewList += result.toString()
    myNewList += " "
}

scala.tools.nsc.io.File("test_output.txt").writeAll(myNewList.toString())
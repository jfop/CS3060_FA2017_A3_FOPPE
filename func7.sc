def func1[A](a: Array[Array[A]], b: Array[Array[A]])(implicit n: Numeric[A]) = {

  import n._

  for (row <- a)
  yield for(col <- b.transpose)
  yield row zip col map Function.tupled(_*_) reduceLeft (_+_)
}

println(func1((Array(Array(1,2,6,4,6))),(Array(Array(3,4,4,7,3,3)))).toSeq)
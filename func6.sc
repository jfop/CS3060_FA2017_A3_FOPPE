def func1(in: Int): Int = {
  in match {
    case a if a <= 0 => 0

    case 0 => 1
    case 1 => 1
	case a => func1(a - 1) + func1(a - 2)
  }
}
println(func1(10))
def func1(n:Int): Int ={
	var before = 0
	var after = 1
	var result = 0
	var x = 0

	while(x < n){
		result = before + after
		before = after
		after = result
		x = x + 1
		}
		return result
}

var a = 0
var result = 0
while(a < 500){
	result = func1(a)
	println(result)
	a = a + 1
}
